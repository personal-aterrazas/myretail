
myRetail Application
=================================
myRetail is a rapidly growing company with HQ in Richmond, VA and over 200 stores across the east coast. myRetail wants
 to expose its product catalog to internal users to enter new products for review and external customers
 and partners for access to product details.  The goal is to build a web instance that satisfies these requirements:

* A Product has four attributes- ID (unique), Title, Description and Price
* Provide a service interface that allows for consumption of active Product details
by any application or site that wants them
* Provide a user interface that displays one or more Products
* BONUS: Provide an interface for the creation and update of potential Products. 



### Architecture
The myRetail side web application and REST API running the play framework.  The following is the myRetail stack:

#### Web Server
myRetail runs on the JVM via the Play framework.  The Play framework is lightweight, stateless, 
minimal resource consumption framework running on the Jboss Netty web server.

#### ORM
myRetail uses Ebean as the ORM to the H2 database.  Ebean is a simple ORM written in Java.

#### Data Store
myRetail uses an in-memory H2 database for development.  myRetail can be configured to use H2 as a server or any 
other production database.

#### Front-End
myRetail uses AngularJS, Bootstrap, and Jquery on the frontend.  The project was created with Yeoman scaffolding which 
includes grunt and bower for build and dependency management.


### RESTful API

| Method     | URI                                    | 
| --------   |----------------------------------------|
| GET        | /api/v1/products                       | 
| GET        | /api/v1/products/:id                   |
| POST       | /api/v1/products                       |
| PUT        | /api/v1/products/:id                   |
| DELETE     | /api/v1/products/:id                   |


### How to Run (On Osx translate commands to appropriate OS)
1. [Install the JDK 1.8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
2. [Install node and npm](https://nodejs.org/en/download/)
3. [Download the Play framework 2.4.2](https://downloads.typesafe.com/typesafe-activator/1.3.6/typesafe-activator-1.3.6-minimal.zip)
4. [Install the Play Framework 2.4.2](https://www.playframework.com/documentation/2.4.2/Installing)
5. cd myretail/ui
6. npm install
7. node_modules/bower/bin/bower install
8. cd ../
9. activator run
10. Open up browser to http://localhost:9000/
11. A database evolution script will need to be run.  Press "Apply this script now"
12. To stop server ctrl-D in command prompt


