'use strict';

/**
 * @ngdoc function
 * @name uiApp.controller:ProductCtrl
 * @description
 * # ProductCtrl
 * Product management controller.
 *
 */
angular.module('myRetailApp')
  .controller('ProductCtrl', function ($scope, Product) {

    $scope.products = [];
    $scope.showModal = false;

    /**
     * Toggle the modal with the appropriate action on the save button.
     *
     * @param action
     */
    $scope.toggleModal = function(action){
      //action will determine what the modal action is taken on save.
      $scope.action = action;
      $scope.showModal = !$scope.showModal;
    };


    /**
     * Determine what server action should be taken based on the modal action set.
     *
     * @param action
     */
    $scope.modalAction = function(action){

      switch(action){
        case 'add':
          $scope.add();
          break;
        case 'update':
          $scope.update();
          break;
      }
    };


    /**
     * Load products from server.
     *
     */
    $scope.loadProducts = function() {
      Product.get().then(
        function (result) {
          $scope.products = result.data;
        },
        function (error) {
          var loadProductError = "failed to retrieve products - " + error.statusText;
          console.error(loadProductError);
          $scope.error = loadProductError;
        }
      );
    };


    /**
     * Show the modal specifically for adding a new product.
     *
     */
    $scope.showAddModal = function(){

      $scope.productTitle = null;
      $scope.productDescription = null;
      $scope.productPrice = null;
      $scope.productId = null;
      $scope.productActive = null;

      //show add product modal
      $scope.toggleModal("add");

    };


    /**
     * Show the modal specifically for the update case.
     *
     * @param product
     */
    $scope.showUpdateModal = function(product){

      //update modal fields with current product
      $scope.productTitle = product.title;
      $scope.productDescription = product.description;
      $scope.productPrice = product.price;
      $scope.productId = product.id;
      $scope.productActive = product.active;

      //show modal
      $scope.toggleModal("update");

    };


    /**
     * Update product on server.
     *
     */
    $scope.update = function(){

      //define payload
      var formFields = {};
      formFields.title = $scope.productTitle;
      formFields.description = $scope.productDescription;
      formFields.price = parseFloat($scope.productPrice);
      formFields.active = $scope.productActive;

      //save product
      Product.update($scope.productId,formFields).then(
        function(result){
          $scope.showModal = false;
          //reload products
          $scope.loadProducts();
        },
        function(error){
          console.error("failed to create product - " + error.statusText);
        }
      );
    };


    /**
     * Delete the product from the server.
     *
     * @param id
     */
    $scope.delete = function(id){

      //delete product
      Product.delete(id).then(
        function(result){
          $scope.showModal = false;
          //reload products
          $scope.loadProducts();
        },
        function(error){
          console.error("failed to delete product - " + error.statusText);
        }
      );
    };


    /**
     * Make the product active.
     *
     * @param id
     * @param activate
     */
    $scope.toggleActivate = function(id,activate){
      //define payload
      var formFields = {};
      formFields.active = activate;

      //save product
      Product.update(id,formFields).then(
        function(result){
          $scope.showModal = false;
          //reload products
          $scope.loadProducts();
        },
        function(error){
          console.error("failed to create product - " + error.statusText);
        }
      );
    };


    /**
     * Add new product.
     *
     */
    $scope.add = function(){

      var formFields = {};
      formFields.title = $scope.productTitle;
      formFields.description = $scope.productDescription;
      formFields.price = parseFloat($scope.productPrice);
      formFields.active = $scope.productActive;

      //save product
      Product.create(formFields).then(
        function(result){
          $scope.showModal = false;
          //reload products
          $scope.loadProducts();
        },
        function(error){
          console.error("failed to create product - " + error.statusText);
        }
      );
    };


    //initialize view
    $scope.loadProducts();

  });
