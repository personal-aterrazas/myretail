angular.module('myRetailApp')
  .factory("Product", ['$http',function($http) {
    return {
      /**
       * Create a new product on the server.
       *
       * @param formFields
       * @returns {*}
       */
      create: function(formFields) {
        return $http.post('/api/v1/products',
          formFields).
          success(function(response)
          {
            return response;
          });
      },
      /**
       * Returns all products.
       *
       * @param id
       * @param title
       * @param description
       * @returns {*}
       */
      get:function(id,title,description) {
        return $http.get('/api/v1/products').
          success(function(response)
          {
            return response;
          });
      },
      /**
       * Updates product on the server.
       *
       * @param id
       * @param formFields
       * @returns {*}
       */
      update:function(id,formFields) {
        return $http.put('/api/v1/products/' + id,
        formFields).
          success(function(response)
          {
            return response;
          });
      },
      /**
       * Deletes specific product.
       *
       * @param id
       * @returns {*}
       */
      delete:function(id) {
        return $http.delete('/api/v1/products/' + id).
          success(function(response)
          {
            return response;
          });
      }
    };
  }]);
