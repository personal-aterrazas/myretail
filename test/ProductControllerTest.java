import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.junit.*;

import play.libs.Json;
import play.libs.ws.WS;
import play.libs.ws.WSResponse;

import static play.test.Helpers.*;
import static org.junit.Assert.*;

public class ProductControllerTest {

    public static final String REST_URL = "http://localhost:3333/api/v1/";
    public static final String URI = "products";
    public static final int TIMEOUT = 3000;

    static ObjectNode product;

    @BeforeClass
    public static void oneTimeSetup(){

        String title = "Teddy Bear";
        String description = "Plush teddy";
        double price = 12.34;
        boolean active = true;

        //test product
        product = Json.newObject();
        product.put("title",title);
        product.put("description",description);
        product.put("price",price);
        product.put("active",active);

    }


    /**
     * Test the API that returns all products.
     *
     */
    @Test
    public void getProducts() {
        running(testServer(3333), () -> {
            //Should return a 200 OK when the outlet is created
            WSResponse response = WS.url(REST_URL + URI).post(product).get(TIMEOUT);

            assertEquals(response.getStatus(),OK);

            //Should return a 200 OK when the outlet is created
            response = WS.url(REST_URL + URI).get().get(TIMEOUT);

            assertEquals(response.getStatus(), OK);

            JsonNode responseNode = Json.parse(response.getBody());
            assertTrue(responseNode.isArray());
            assertTrue(responseNode.size() > 0);

        });
    }


    /**
     * Test the API that creates a product.
     *
     */
    @Test
    public void createProduct() {
        running(testServer(3333), () -> {
            //Should return a 200 OK when the outlet is created
            WSResponse response = WS.url(REST_URL + URI).post(product).get(TIMEOUT);

            assertEquals(response.getStatus(),OK);
        });
    }

}
