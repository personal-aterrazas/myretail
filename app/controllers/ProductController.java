package controllers;

import com.avaje.ebean.Ebean;
import com.fasterxml.jackson.databind.JsonNode;
import models.Product;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;

import java.util.List;

/**
 * Product Controller exposes Product REST API.
 *
 */
public class ProductController extends Controller {

    //Json product keys
    static final String title = "title";
    static final String description = "description";
    static final String price = "price";
    static final String active = "active";

    //standard error messages
    static final String REQUIRED_FIELDS_ERROR = "Required fields were not provided.";
    static final String UNEXPECTED_ERROR = "An error occurred attempting to create a product";
    static final String NOT_FOUND = "Product not found";


    /**
     * Handles the adding of a new product.
     *
     * @return
     */
    public Result createProduct(){

        Http.Request rq = request();
        JsonNode rqJson = rq.body().asJson();

        Product product;
        if(validateCreate(rqJson)){

            product = new Product();
            product.setTitle(rqJson.get(title).asText());
            product.setDescription(rqJson.get(description).asText());
            product.setPrice(rqJson.get(price).asDouble());
            product.setActive(rqJson.get(active).asBoolean());

            Ebean.save(product);
        }else{
            return status(400,REQUIRED_FIELDS_ERROR);
        }

        if(product == null){
            return status(500,UNEXPECTED_ERROR);
        }

        return status(200,Json.toJson(product.getId()));
    }


    /**
     * Validate all required fields are provided.
     *
     * @param requestBody
     * @return
     */
    private boolean validateCreate(JsonNode requestBody){
        if(requestBody.get(title) != null && requestBody.get(description) != null &&
                requestBody.get(price) != null){
            //valid
            return true;
        }

        //invalid
        return false;
    }


    /**
     * Returns a list of all products based on the search criteria parameters.
     *
     *
     * @return
     */
    public Result getProducts(){

        List<Product> products = Ebean.find(Product.class).findList();

        return status(200,Json.toJson(products));
    }


    /**
     * Delete product by id.
     *
     * @param id
     * @return
     */
    public Result deleteProduct(Long id){

        Product product = Ebean.find(Product.class, id);

        if(product == null){
            return status(404, NOT_FOUND);
        }

        //delete product
        Ebean.delete(product);

        //delete return no content status code
        return status(204);
    }


    /**
     * Returns a specific product by id.
     *
     * @param id
     * @return
     */
    public Result getProduct(Long id){

        Product product = Ebean.find(Product.class, id);

        if(product == null){
            return status(404, NOT_FOUND);
        }

        //product found
        return status(200,Json.toJson(product));
    }


    /**
     * Updates Product with attributes provided.
     *
     * @param id
     * @return
     */
    public Result updateProduct(Long id) {

        Http.Request rq = request();
        JsonNode rqJson = rq.body().asJson();

        //find product to update
        Product product = Ebean.find(Product.class, id);

        if(product == null){
            return status(404, NOT_FOUND);
        }

        //update title
        if(rqJson.get(title) != null){
            product.setTitle(rqJson.get(title).asText());
        }

        //update description
        if (rqJson.get(description) != null){
            product.setDescription(rqJson.get(description).asText());
        }

        //update price
        if (rqJson.get(price) != null){
            product.setPrice(rqJson.get(price).asDouble());
        }

        //update active
        if (rqJson.get(active) != null){
            product.setActive(rqJson.get(active).asBoolean());
        }

        //update product
        try{
            Ebean.save(product);
        }catch(Exception e){
            return status(500,e.getMessage());
        }

        //success return id of updated product
        return status(200,Json.toJson(product.getId()));
    }

}
